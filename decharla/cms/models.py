from django.db import models
import datetime


# Create your models here.


class User(models.Model):
    name = models.CharField(max_length=64, default="Anonimo")
    password = models.CharField(max_length=16, default="")
    cookie = models.CharField(max_length=64)
    font = models.CharField(max_length=50, default='Arial')
    size_font = models.CharField(max_length=50, default='Normal')

    def __str__(self):
        return f"{str(self.id)}: {self.name}"

class Room(models.Model):
    name = models.CharField(max_length=64)  # Campo de caracteres de max 64 bites, limitandolo se aceleran las cosas
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    # valor = models.TextField()
    # tema = models.ManyToManyField(Tema)
    creation_date = models.DateTimeField()
    def __str__(self):
        return f"{str(self.id)}: {self.name}"


class Message(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    message = models.TextField()
    image = models.BooleanField(default=False)
    date = models.DateTimeField()

    # titulo = models.CharField(max_length=128)  # Campo de caracteres de max 64 bites, limitandolo se aceleran las cosas

    def __str__(self):
        return f"{str(self.id)}: {self.message} ~~~{self.room.name}"

    def verificar(self):
        return ("cuerpo" in self.message)

class RoomAccess(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(null=True)

    def __str__(self):
        return f"{self.room}: {self.date} ~~~{self.user}"

class LikeMsg(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    msg = models.ForeignKey(Message, on_delete=models.CASCADE)
    liked = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.id}: {self.room.name}---{self.msg.message} ~~~{self.user.name}"

class LikeRoom(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    liked = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.id}: {self.room.name}"
