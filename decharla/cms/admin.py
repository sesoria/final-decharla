from django.contrib import admin

# Register your models here.
from .models import Room, Message, User, RoomAccess, LikeMsg, LikeRoom

admin .site.register(Room)
admin .site.register(Message)
admin .site.register(User)
admin .site.register(RoomAccess)
admin .site.register(LikeRoom)
admin .site.register(LikeMsg)