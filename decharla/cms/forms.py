from django import forms

from .models import Room, Message, User

class PasswordForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ("password",)
        widgets = {
            "password": forms.PasswordInput()
        }

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ("name", "font", "size_font")
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control me-2 my-select'}),
        }

    def __init__(self, *args, **kwargs):
        initial = kwargs.get('initial', {})
        initial.setdefault('name', 'Anonimo')
        initial.setdefault('font', 'Arial')
        initial.setdefault('size_font', 'Normal')

        if 'instance' in kwargs:
            instance = kwargs['instance']
            initial['name'] = instance.name
            initial['font'] = instance.font
            initial['size_font'] = instance.size_font
        
        kwargs['initial'] = initial
        super().__init__(*args, **kwargs)



class RoomForm (forms.ModelForm):
    class Meta:
        model = Room
        fields = ("name",)
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control me-2 my-border', 'placeholder': 'Crear nueva sala'}),
        }

class MessageForm (forms.ModelForm):
    class Meta:
        model = Message
        fields = ("message", "image",)
        widgets = {
            'message': forms.Textarea(attrs={'class': 'form-control', 'rows': '2'}),
        }