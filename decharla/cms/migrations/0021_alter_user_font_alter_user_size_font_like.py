# Generated by Django 4.1.7 on 2023-06-13 19:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("cms", "0020_alter_user_font_alter_user_size_font"),
    ]

    operations = [
        migrations.AlterField(
            model_name="user",
            name="font",
            field=models.CharField(default="Arial", max_length=50),
        ),
        migrations.AlterField(
            model_name="user",
            name="size_font",
            field=models.CharField(default="Normal", max_length=50),
        ),
        migrations.CreateModel(
            name="Like",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("date", models.DateTimeField(null=True)),
                (
                    "message",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="cms.message"
                    ),
                ),
                (
                    "room",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="cms.room"
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="cms.user"
                    ),
                ),
            ],
        ),
    ]
