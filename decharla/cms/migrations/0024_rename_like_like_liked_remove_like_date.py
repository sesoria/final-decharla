# Generated by Django 4.1.7 on 2023-06-14 14:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("cms", "0023_rename_message_like_msg"),
    ]

    operations = [
        migrations.RenameField(
            model_name="like",
            old_name="like",
            new_name="liked",
        ),
        migrations.RemoveField(
            model_name="like",
            name="date",
        ),
    ]
