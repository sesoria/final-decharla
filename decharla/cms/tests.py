from django.test import TestCase
from cms.models import User
from django.urls import reverse

class E2ETestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.user = User.objects.create(password='1234')

    def test_main(self):
        # Test 1: Login
        login_url = reverse('login')
        response = self.client.post(login_url, {'password': '1234'})
        self.assertEqual(response.status_code, 302)

        # Test 2: Create room
        index_url = reverse('index')
        valid_room_name = "test room"
        response = self.client.post(index_url, {'name': valid_room_name})
        self.assertEqual(response.status_code, 200)

        # Test 3: Request existing room
        manage_room_url = reverse('manage_room', kwargs={'resource': valid_room_name})
        response = self.client.get(manage_room_url)
        self.assertEqual(response.status_code, 200)

        # Test 4: Request non existing room
        not_valid_room = 'non existing room'
        manage_room_url = reverse('manage_room', kwargs={'resource': not_valid_room})
        response = self.client.get(manage_room_url)
        self.assertEqual(response.status_code, 404)

        # Test 5: Post new message
        manage_room_url = reverse('manage_room', kwargs={'resource': valid_room_name})
        response = self.client.post(manage_room_url, {'message': 'HOLA este es mi mensaje'})
        self.assertEqual(response.status_code, 200)

        # Test 6: Post message to non existing room
        manage_room_url = reverse('manage_room', kwargs={'resource': not_valid_room})
        response = self.client.get(manage_room_url)
        self.assertEqual(response.status_code, 404)
