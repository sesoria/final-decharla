from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404, JsonResponse, HttpResponseForbidden
from .models import Room, User, Message, RoomAccess, LikeMsg, LikeRoom
from django.template import loader
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.core import serializers
from .forms import RoomForm, MessageForm, UserForm, PasswordForm
from random import randint
from xml.dom.minidom import parseString
# Create your views here.

def get_new_msgs(room, user):
    try:
        last_access = RoomAccess.objects.get(room=room, user=user)
        new_msgs = Message.objects.filter(room=room, date__gt=last_access.date).count()
    except RoomAccess.DoesNotExist:
        new_msgs = Message.objects.filter(room=room).count()
    return new_msgs


def rooms_parameters(user):
    total_messages = 0
    total_photos = 0
    rooms = Room.objects.all()
    new_msgs = {}
    messages_dict = {}
    for room in rooms:
        room_messages = Message.objects.filter(room=room)
        new_msgs[room.name] = get_new_msgs(room, user)  # obtener la cantidad de mensajes no vistos de cada sala
        messages_dict[room.name] = room_messages.count()  # obtener numero mensajes de cada sala
        total_messages += room_messages.filter(image=False).count()  # obtener numero de mensajes de texto
        total_photos += room_messages.filter(image=True).count()  # obtener numero de mensajes imagen

    return total_messages, total_photos, new_msgs, messages_dict


def sort_rooms_by_likes(rooms, likes_ids):
    rooms = Room.objects.all()  # obtener todas las salas
    sort_ids = sorted(likes_ids, key=likes_ids.get, reverse=True)
    x = sorted(rooms, key=lambda x: sort_ids.index(x.id) if x.id in sort_ids else len(sort_ids))
    return x


def show_rooms(request, user):
    form = RoomForm()  # obtener el formulario de room
    ip_addres = request.META.get('REMOTE_ADDR')   # obtener la ip del servicio
    port = request.META.get('SERVER_PORT')  # obtener el puerto del servicio
    rooms = Room.objects.all()  # obtener todas las salas
    # obtener la cantidad de mensajes y fotos totales y de cada sala
    total_messages, total_photos, new_msgs, messages_dict = rooms_parameters(user)
    like_ids = LikeRoom.objects.values_list('room', flat=True).distinct() # Se obtienen los likes sin repetir
    likes = {}
    for id in like_ids:
        likes[id] = LikeRoom.objects.filter(room=id, liked=True).count()
    sort_rooms = sort_rooms_by_likes(rooms, likes)
    context = {
        "user": user,
        "rooms": sort_rooms,
        "form": form,
        "address": ip_addres,
        "port": port,
        "num_msgs": messages_dict,
        "new_msgs": new_msgs,
        "total_messages": total_messages,
        "total_photos": total_photos,
        "likes": likes
    }
    return render(request, "rooms.html", context)


def room_likes(request, user):
    if "liked" in request.POST:
        liked = request.POST['liked']
        id_room = request.POST['id_room']
        room = Room.objects.get(id=id_room)  # Obtenemos la Sala por el id obtenido en el boton
        try:
            like = LikeRoom.objects.get(room=id_room, user=user)
            like.liked = not like.liked
            like.save()
        except LikeRoom.DoesNotExist:
            LikeRoom(room=room, user=user, liked=liked).save()

@csrf_exempt
def new_room(request, user):
    form = RoomForm(request.POST)
    if form.is_valid():  # Check --> CharField? IntegerField?
        room = form.save(commit=False)  # Obtener instancia del modelo sin guardar en la base de datos
        try:
            Room.objects.get(name=room.name)  # Los nombres de sala son unicos e identificativos
            rooms = Room.objects.all()
            form = RoomForm()
            form.errors['name'] = ['Esa sala ya existe usa otro nombre']
            # obtener la cantidad de mensajes y fotos totales y de cada sala
            total_messages, total_photos, new_msgs, messages_dict = rooms_parameters(user)
            context = {
                "form": form,
                "rooms": rooms,
                "total_messages": total_messages,
                "total_photos": total_photos,
                "user": user
            }
            return render(request, "rooms.html", context)
        except Room.DoesNotExist:
            room.author = user
            room.creation_date = timezone.now()
            room.save()  # Guardar el nuevo objeto del modelo en la base de datos
    room_likes(request, user)

    return show_rooms(request, user)


def index(request):
    user = check_user(request)
    if not user:
        return redirect("/login")
    elif request.method == "POST":
        return new_room(request, user)
    elif request.method == "GET":
        return show_rooms(request, user)


def logIn(request):
    if request.method == "POST":
        form = PasswordForm(request.POST)
        if form.is_valid():  # Check --> CharField? IntegerField?
            try:
                form = form.save(commit=False)
                user = User.objects.get(password=form.password)
                cookie = randint(0, 10000)
                response = redirect("/")
                response.set_cookie('mi_cookie', f'{cookie}')
                user.cookie = cookie
                user.save()
                return response

            except User.DoesNotExist:
                form = PasswordForm()
                form.errors['password'] = ['Contraseña incorrecta']
                context = {
                    "form": form
                }
                return render(request, "registration/login.html", context, status=401)

    form = PasswordForm()
    context = {
        "form": form
    }
    return render(request, "registration/login.html", context)


"""
Get the IP address and port of the server where the app is running to use them for redirecting to the main page using a 
template. Also deletes the session cookie 'mi_cookie'
"""
def logOut(request):
    ip_addres = request.META.get('REMOTE_ADDR')
    port = request.META.get('SERVER_PORT')
    context = {
        "address": ip_addres,
        "port": port
    }
    response = render(request, "registration/logged_out.html", context)
    response.delete_cookie("mi_cookie")
    return response


def config(request):
    user = check_user(request)
    if not user:
        return redirect("/login")
    elif request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():  # Check --> CharField? IntegerField?
            form = form.save(commit=False)
            user.name = form.name
            user.font = form.font
            user.size_font = form.size_font
            user.save()

    form = UserForm(instance=user)
    # obtener la cantidad de mensajes y fotos totales y de cada sala
    total_messages, total_photos, _, _= rooms_parameters(user)
    context = {
        "user": user,
        "rooms": Room.objects.all(),
        "form": form,
        "total_messages": total_messages,
        "total_photos": total_photos
    }
    return render(request, "config.html", context)


def help(request):
    user = check_user(request)
    if not user:
        return redirect("/login")
    # obtener la cantidad de mensajes y fotos totales y de cada sala
    total_messages, total_photos, _, _= rooms_parameters(user)
    context = {
        "user": user,
        "rooms": Room.objects.all(),
        "total_messages": total_messages,
        "total_photos": total_photos
    }
    return render(request, "help.html", context)


def register_access(user, room):
    try:
        access = RoomAccess.objects.get(room=room, user=user)
        access.date = timezone.now()
        access.save()
    except RoomAccess.DoesNotExist:
        RoomAccess(room=room, user=user, date= timezone.now()).save()


def check_user(request):
    authorization_header = request.META.get('HTTP_AUTHORIZATION')
    cookie = request.COOKIES.get('mi_cookie')
    if cookie:
        try:
            user = User.objects.get(cookie=cookie)
        except User.DoesNotExist:
            return None
    else:
        try:
            user = User.objects.get(password=authorization_header)
        except User.DoesNotExist:
            return None

    return user


@csrf_exempt
def xml_message(request, resource, user):
    document = parseString(request.body)
    messages = document.getElementsByTagName('message')
    for message in messages:
        isimg = message.getAttribute('isimg')
        text = message.getElementsByTagName('text')[0].firstChild.nodeValue
        room, _ = Room.objects.get_or_create(name=resource, defaults={"name": resource, "author": user,
                                                                  "creation_date": timezone.now()})
        Message.objects.create(room=room, author=user, image=isimg.capitalize(), message=text,date= timezone.now())
    return get_resource(request, resource, user)


def message_likes(request, user, room):
    if "liked" in request.POST:
        liked = request.POST['liked']
        id_msg = request.POST['id_msg']
        msg = Message.objects.get(id=id_msg)  # Obtenemos el mensaje por el id obtenido en el boton
        try:
            like = LikeMsg.objects.get(msg=id_msg, user=user)
            like.liked = not like.liked
            like.save()
        except LikeMsg.DoesNotExist:
            LikeMsg(room=room, user=user, msg=msg, liked=liked).save()


@csrf_exempt
def new_message(request, resource, user):
    try:
        room = Room.objects.get(name=resource)
    except Room.DoesNotExist:
        return render(request, "cms/404.html", status=404)

    form = MessageForm(request.POST)

    if form.is_valid():  # Check --> CharField? IntegerField?
        message = form.save(commit=False)  # Obtener instancia del modelo sin guardar en la base de datos
        message.author = user
        message.room = room
        message.date = timezone.now()
        message.save()  # Guardar el objeto del modelo en la base de datos
        register_access(user, room)  # Al hacer post no se llama a get_resource y el nuevo mensaje no se registra
    message_likes(request, user, room)

    return get_resource(request, resource, user)

@csrf_exempt
def get_resource(request, resource, user):
    try:
        room = Room.objects.get(name=resource)
    except Room.DoesNotExist:
        return render(request, "cms/404.html", status=404)
    register_access(user, room)

    form = MessageForm()  # obtener el formulario de room
    messages = Message.objects.filter(room= Room.objects.get(name=resource))  # obtener el contendio del template
    ip_addres = request.META.get('REMOTE_ADDR')  # obtener la ip del servicio
    port = request.META.get('SERVER_PORT')  # obtener el puerto del servicio
    # obtener la cantidad de mensajes y fotos totales y de cada sala
    total_messages, total_photos, new_msgs, messages_dict = rooms_parameters(user)
    like_ids = LikeMsg.objects.values_list('msg', flat=True).distinct() # Se obtienen los likes sin repetir
    likes = {}
    for id in like_ids:
        likes[id] = LikeMsg.objects.filter(msg=id, liked=True).count()

    context = {  # crear el context
        "messages": messages,
        "form": form,
        "user": user,
        "address": ip_addres,
        "port": port,
        "room": resource,
        "rooms": Room.objects.all(),
        "total_messages": total_messages,
        "total_photos": total_photos,
        "likes": likes
    }
    return render(request, "room.html", context)

@csrf_exempt
def manage_room(request, resource):
    user = check_user(request)
    if not user:
        return redirect("/login")
    if request.method == "PUT":
        return xml_message(request, resource, user)
    elif request.method == "POST":
        return new_message(request, resource, user)
    elif request.method == "GET":
        return get_resource(request, resource, user)

def get_resource_json(request, resource):

    cookie = request.COOKIES.get('mi_cookie')
    if not cookie:
        return redirect("/login")
    try:
        room = Room.objects.get(name=resource) # VEr si existe la sala
    except Room.DoesNotExist:
        return render(request, "cms/404.html", status=404)

    msgs_room = Message.objects.filter(room=Room.objects.get(name=resource))
    # crear el context
    messages_list = []
    for msg in msgs_room:
        msg_info = {
            'author': msg.author.name,
            'text': msg.message,
            'isimg': msg.image,
            'date': msg.date
        }
        messages_list.append(msg_info)

    # -- creación de un diccionario con la información de la sala, es decir nombre y mensajes
    room_info = {
        'roomName': room.name,
        'messages': messages_list
    }

    return JsonResponse(room_info, status=200, safe=False, json_dumps_params={'indent': 4})

def dynamic_resource(request, resource):
    user = check_user(request)
    if not user:
        return redirect("/login")
    try:
        room = Room.objects.get(name=resource) # VEr si existe la sala
    except Room.DoesNotExist:
        return render(request, "cms/404.html", status=404)
    form = MessageForm()  # obtener el formulario de room
    ip_addres = request.META.get('REMOTE_ADDR')  # obtener la ip del servicio
    port = request.META.get('SERVER_PORT')  # obtener el puerto del servicio
    # obtener la cantidad de mensajes y fotos totales y de cada sala
    total_messages, total_photos, new_msgs, messages_dict = rooms_parameters(user)
    context = {  # crear el context
        "rooms": Room.objects.all(),
        "room": room,
        "form": form,
        "address": ip_addres,
        "port": port,
        "user": user,
        "num_msgs": messages_dict,
        "new_msgs": new_msgs,
        "total_messages": total_messages,
        "total_photos": total_photos
    }
    return render(request, "dynamic_room.html", context)