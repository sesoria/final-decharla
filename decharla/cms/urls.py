from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.logIn, name="login"),
    # path('logged', views.logged),
    path('logout', views.logOut),
    path('config', views.config),
    path('help', views.help),
    path('<str:resource>.json', views.get_resource_json, name= "json"),
    # path('<str:resource>.xml', views.get_resource_xml, name= "xml"),
    path('dynamic_<str:resource>', views.dynamic_resource, name= "dynamic"),
    path('<str:resource>', views.manage_room, name= "manage_room"),

]