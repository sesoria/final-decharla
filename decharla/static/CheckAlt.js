          document.addEventListener('error', function(event) {
          var target = event.target;
          if (target.tagName === 'IMG') {
            var altText = target.getAttribute('alt');
            var icon = document.createElement('i');
            icon.classList.add('fa-solid', 'fa-image');
            icon.style.color = '#26a269';
            if (altText) {
              target.parentNode.insertBefore(icon, target.nextSibling);
            }
          }
        }, true);