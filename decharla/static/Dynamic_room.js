// Retrieve a JSON file every some time, and fill in a <div> element with it
//

function getMessages (url, container) {
  fetch(url).then(response => {
    console.log("Response received!", response)
    if (!response.ok) {
      throw new Error("HTTP error " + response.status);
    }
    return response.json();
 }).then(messages => {
    messages_str = ""
    messages.messages.forEach(message => {
      message_str = '<li class="list-group-item list-group-item-secondary">'
      message_str += `<span type="button" class="small-button btn btn-success">${message.author}</span>`
      if (message.isimg) {
        // message_str += `<img src=${message.text} alt="Imagen">`
        message_str += `<span class="list-group-item bg-light my-navbar">
                        <img class="room-image" src="${message.text}" alt="Imagen ">
                      </span>`
      } else {
        // message_str += message.text;
        message_str += `<span class="list-group-item bg-light my-navbar">${message.text}
        </span>
        `
      }
      message_str += "</li>"
  
      messages_str += message_str
    });
    container.innerHTML = messages_str;
 }).catch(function () {
    console.log("Error decoding JSON")
 })
}

window.addEventListener("DOMContentLoaded", (event) => {
  console.log("DOM fully loaded and parsed");
  const container = document.querySelector('#messages');
  getMessages(url_messages, container);
  setInterval(() => {
    console.log("Interval");
    getMessages(url_messages, container);
    }, 5 *1000); // Call every 5 seconds
});