document.addEventListener('DOMContentLoaded', (event) => {
    const searchInput = document.getElementById('search-input');
    const searchButtons = document.getElementsByClassName('search-button');

    Array.from(searchButtons).forEach(function(searchButton) {
        searchButton.addEventListener('click', function(event) {
            event.preventDefault();
            const searchUrl = searchButton.getAttribute('data-search-url');
            if (searchUrl) {
                window.location.href = searchUrl;
            } else {
                const searchValue = searchInput.value;
                window.location.href = searchValue;
            }
        });
    });
});
