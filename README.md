# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Francisco David Soria Erazo
* Titulación: Ingenieria Telematica
* Cuenta en laboratorios: sesoria
* Cuenta URJC: fd.soria.2020
* * Video parte basica + parte opcional (url): https://youtu.be/ga_11w9aysc
* Despliegue (url): http://34.245.152.159/
* Contraseñas: xx34d23, popi, django, piropi, 1234
* Cuenta Admin Site: admin / admin

## Resumen parte obligatoria

Para acceder a cualquier recurso de la pagina hay que loguearse, de otro modo se te redirige al login.

La pagina principal muestra una lista de las info de las salas ( nºlikes, nombre, mensajes totales y no vistos, creador y fecha de creacion) ordenadas segun su numero de likes. Se puede crear una sala o acceder a alguna mediante un buscador.

En cada sala se muestran los diferentes mensajes enviados junto con el nombre de su creador, tu nombre aparece en azul. Tambien aparece un boton de like con el nº de likes del mensaje, si le das de nuevo al boton le quitas el like y el nº decrece. La sala tiene un boton para acceder a sus mensajes en Json, asi como para acceder a la sala dinamica. Los mensajes enviados pueden ser imagenes si se rellena la checkbox del formulario del mensaje, si no se introduce un formato de url correcto para la imagen se muestra "Image" junto a un icono para dar a entender que no se pudo resolver la imagen.

En configuracion podemos cambiar nuestro nombre, el tamaño de la letra y el tipo de letra entre las opciones predeterminadas.

En logout puedes cerrar sesion para loguearte con otra cuenta.

En todas los recursos aparece una Cabecera con el nombre de la pagina que al clickar se nos redirige a la pagina principal. Tambien aparece un menu con cada uno de los recursos principales junto a un icono identificativo que redirige al recurso en cuestion. Hay un pie de pagina con informacion sobre la pagina (Salas, mensajes de texto e imagenes totales)

## Lista partes opcionales

* Favicon: favicon. He incluido el favicon de la pagina.
* Likes: Hay un sistema de likes por el cual se muestran las salas mas votadas primero, se pueden votar mensajes tambien, se puede retirar el like.
* Test Extremo a Extremo: Tests extremo a extremo del proceso de login, creacion de sala y envio de mensaje. 
* Logout: Se puede cerrar la sesion con un boton. 
* Responsive: La pagina tiene elementos resposive.
* Despligue en AWS
